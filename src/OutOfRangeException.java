/**
 * Exception caught when user's input is out of range
 * @author NDIAYE Antoine Latgrand
 * @version 1.0
 */
public class OutOfRangeException extends Exception {


	private static final long serialVersionUID = -3735174557656347172L;

	public OutOfRangeException() {
		super("Warning, the specified floor is out of range. ");
	}
}
