/**
 * Class which allows to test the elevator
 * @author antoine
 * @version 1.0
 */
public class Test {

	public static void main(String[] args) {
		Elevator e = new Elevator();
		e.askUser();
	}
}
