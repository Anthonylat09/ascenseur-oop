import java.util.Scanner;

/**
 * This class represents an elevator, which users can call and use to go up or down in a building
 * @author NDIAYE Antoine Latgrand
 * @version 1.0
 */
public class Elevator implements ElevatorInterface{

	//Constructors
	
	public Elevator() {
		super();
	}
	
	//Variables
	
	int currentFloor = 0;
	
	//Methods
	
	public void askUser() {
		System.out.println("Current Floor: "+ currentFloor);
		System.out.print("Enter destination floor: ");
		try (Scanner sc = new Scanner(System.in)) {
			int destinationFloor = sc.nextInt();
			if (destinationFloor < minFloor || destinationFloor > maxFloor) {
				try {
					throw new OutOfRangeException();
				} catch (OutOfRangeException e) {
					e.printStackTrace();
				}
				Thread.sleep(100);
				askUser();
			}
			else {
				System.out.println("Door closing...");
				Thread.sleep(500);
				if (destinationFloor < currentFloor) {
					goDown(destinationFloor);
				}
				else if (destinationFloor > currentFloor) {
					goUp(destinationFloor);
				}
				System.out.println("Door opening...");
				askUser();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void goUp(int destinationFloor) {
		try {
			while (currentFloor++ < destinationFloor -1) {
				System.out.println("Going up... Current floor: "+currentFloor+" | Destination Floor: "+destinationFloor);
				Thread.sleep(500);
			}
			System.out.println("Going up... Current floor: "+currentFloor);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void goDown(int destinationFloor) {
		try {
			while (currentFloor-- > destinationFloor + 1) {
				System.out.println("Going down... Current floor: "+currentFloor+" | Destination Floor: "+destinationFloor);
				Thread.sleep(500);
			}
			System.out.println("Going down... Current floor: "+currentFloor);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
}
