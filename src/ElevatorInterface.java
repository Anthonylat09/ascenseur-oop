
/**
 * Interface that specifies the needs for the Elevator Class
 * @author NDIAYE Antoine Latgrand
 * @version 1.0
 */
public interface ElevatorInterface {

	public static final int maxFloor = 10;
	public static final int minFloor = 0;
	
	/**
	 * This function asks the user to which floor he wishes to go
	 * after going in the elevator
	 */
	public void askUser();
	
	/**
	 * This function makes the elevator go up to the specified
	 * floor
	 * @param destinationFloor
	 */
	public void goUp(int destinationFloor);
	
	/**
	 * This function makes the elevator go down to the specified 
	 * floor
	 * @param destinationFloor
	 */
	public void goDown(int destinationFloor);
}
