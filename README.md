# Procédure de lancement du projet

Ce projet a été créé en utilisant Java. Pour lancer le projet, on peut l'ouvrir dans un IDE Java( Eclipse IDE ou IntelliJ par exemple) puis appuyer sur le bouton dédié au lancement. <br>

Sur une distribution Linux, on peut lancer simplement en ligne de commande: <br>
    $ sudo apt install default-jre

pour installer java s'il n'est pas déja présent sur le poste, puis: <br>
    $ cd bin && java Test

pour lancer le projet dans un terminal.
